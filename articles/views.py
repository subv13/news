# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from articles.models import Article


def home_view(request):
    articles = Article.objects.order_by('created_at')[:5]
    return render(request, 'articles/index.html', {
        'articles': articles
    })

