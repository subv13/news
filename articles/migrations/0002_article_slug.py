# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-06-01 10:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='slug',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
